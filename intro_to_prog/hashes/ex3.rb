name_and_age = {name: "Bob", age: 20}
name_and_age.each_key { |name| puts name }
puts
name_and_age.each_value { |age| puts age }
puts
name_and_age.each {|key, value| puts "This is the key: #{key}. This is the value: #{value}."}
