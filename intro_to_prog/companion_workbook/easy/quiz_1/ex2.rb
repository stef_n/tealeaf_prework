# ! is the bang operator
# ? is a conditional expression

# 1. what is != and where should you use it?
#   Checks to see if values of two things are equal or not...if not, then it is true.
# 2. put ! before something, like !user_name
#   Turns it into the opposite

# 3. put ! after something, like words.uniq!
#   part of the syntax, in particaluar destorys the repeating words.

# 4. put ? before something
#   ternary operator for if...else
# 5. put ? after something
#   part of the syntax
# 6. put !! before something, like !!user_name
#   Turns it into the boolean equivalent
