################## A

def mess_with_vars(one, two, three)
    one = two
    two = three
    three = one
end

  one = "one"
  two = "two"
  three = "three"

  mess_with_vars(one, two, three)

  puts "one is: #{one}"
  puts "two is: #{two}"
  puts "three is: #{three}"

# I believe it goes with the local variable, since it is defined as a string.
# one is one
# two is two
# three is three

################## B

 def mess_with_vars(one, two, three)
    one = "two"
    two = "three"
    three = "one"
end

  one = "one"
  two = "two"
  three = "three"

 mess_with_vars(one, two, three)

  puts "one is: #{one}"
  puts "two is: #{two}"
  puts "three is: #{three}"


# Looks like that reasoning is not correct...but it does seem to be ignorning the variables in the method, either way.
# Answer is the same as A.


################## C

def mess_with_vars(one, two, three)
    one.gsub!("one","two")
    two.gsub!("two","three")
    three.gsub!("three","one")
  end

  one = "one"
  two = "two"
  three = "three"

  mess_with_vars(one, two, three)

  puts "one is: #{one}"
  puts "two is: #{two}"
  puts "three is: #{three}"

  # Well, .gsub! forced the second argument to return...
  # so, we get what we want....
  # one is two
  # two is three
  # three is one
