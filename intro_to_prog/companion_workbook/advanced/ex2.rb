greetings = { a: 'hi' }
informal_greeting = greetings[:a]
informal_greeting << ' there'

puts informal_greeting  #  => "hi there"
puts greetings  # => "{"hi there"} because the informal_greeting variable
# appends to the greeting ' there'
