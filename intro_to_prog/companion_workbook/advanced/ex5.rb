def dot_separated_ip_address?(input_string)
  dot_separated_words = input_string.split(".")
  return false unless dot_separated_words.size == 4

  while dot_separated_words.size > 0 do
    word = dot_separated_words.pop
    return false unless word.to_f
  end


  true
end



puts  "4.5.5 is a #{dot_separated_ip_address?('4.5.5')} IP."
puts  "1.2.3.4.5 is a #{dot_separated_ip_address?('1.2.3.4.5')} IP."
puts  "10.4.5.11 is a #{dot_separated_ip_address?('10.4.5.11')} IP."

# Could not get it to test properly on the .is_a_number?, so I changed
# it to a .to_f...
# Not sure what the issue was?
