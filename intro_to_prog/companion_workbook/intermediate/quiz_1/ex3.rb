# puts "the value of 40 + 2 is " + (40 + 2)
# error because you are trying to add integers and a string.
#
# Possible solutions:

puts "the value of 40 + 2 is " + (40 + 2).to_s
puts "----Or----"
puts "the value of 40 + 2 is #{40 + 2}"

