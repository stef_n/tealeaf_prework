munsters = {
  "Herman" => { "age" => 32, "gender" => "male" },
  "Lily" => { "age" => 30, "gender" => "female" },
  "Grandpa" => { "age" => 402, "gender" => "male" },
  "Eddie" => { "age" => 10, "gender" => "male" },
  "Marilyn" => { "age" => 23, "gender" => "female"}
}

munsters.each do |n, group|
  case group["age"]
    when 0..18
      group["age_group"] = "kid"
    when 18..65
      group["age_group"] = "adult"
    else
      group["age_group"] = "senior"
  end
end
