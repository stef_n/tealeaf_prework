# Requested to list movies in a hash
movies = { Star_Wars_A_New_Hope:  1977, Raiders_of_the_Lost_Ark:  1981, Back_to_the_Future: 1985, Ferris_Buellers_Day_Off: 1986, Star_Wars_The_Force_Awakens: 2017 }

puts movies [:Star_Wars_A_New_Hope]
puts movies [:Raiders_of_the_Lost_Ark]
puts movies [:Back_to_the_Future]
puts movies [:Ferris_Buellers_Day_Off]
puts movies [:Star_Wars_The_Force_Awakens]

# An array of the release dates
dates = [1977, 1981, 1985, 1986, 2017]
puts
puts dates
