def get_me_to_zero(down)
  return if down.zero?
  puts down
  get_me_to_zero(down - 1)
end

puts get_me_to_zero(10)
puts get_me_to_zero(4)
