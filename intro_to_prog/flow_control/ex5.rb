puts "Tell me a number between 0 and 100!"

n = gets.chomp.to_i

def number(n)
  case n
  when 0.upto(50)
    puts "I forgot to tell you I am a numbers master, #{n} is between 0 and 50."
  when 51.upto(100)
      puts "I forgot to tell you I am a numbers master, #{n} is between 50 and 100."
  else
    if n < 0
       puts "I said between 0 and 100! Get rid of this negativity!"
    else
       puts "I said between 0 and 100! Get these high numbers outta here!"
    end
  end
end

puts number(n)
