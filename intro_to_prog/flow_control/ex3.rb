puts "Tell me a number between 0 and 100!"

n = gets.chomp.to_i

if n < 0
  puts "I said between 0 and 100!"
elsif n <= 50
  puts "I forgot to tell you I am a numbers master, #{n} is between 0 and 50."
elsif n <= 100
  puts "I forgot to tell you I am a numbers master, #{n} is between 50 and 100."
else n > 100
  puts "I said between 0 and 100!"
end

